#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include <BMP085.h>
#include "EEPROMAnything.h"

/*
$GPGGA,232000,2358.439,S,04618.474,W,1,08,0.9,545.4,M,46.9,M,,*4A
$GPRMC,232000,A,2358.439,S,04618.474,W,022.4,090.0,050616,003.1,W*6D
$GPGSA,A,3,04,05,,09,12,,,24,,,,,2.5,1.3,2.1*39
*/

#define BT_ESQ_PIN 4 // botao esquerdo
#define BT_DIR_PIN 2 // botao direito
boolean BT_ESQ_PRESTATUS = false;
boolean BT_DIR_PRESTATUS = false;
boolean BT_ESQ_STATUS = false;
boolean BT_DIR_STATUS = false;

#define GPSRX 12
#define GPSTX 13
SoftwareSerial ss(GPSRX, GPSTX);
//#define ss Serial

#define Display_SCLK 8 // Serial clock out (SCLK)
#define Display_DIN 7  // Serial data out (DIN)
#define Display_DC 6   // Data/Command select (D/C)
#define Display_CS 5   // LCD chip select (CS/CE)
#define Display_RST 9  // LCD reset (RST)

Adafruit_PCD8544 display = Adafruit_PCD8544(Display_SCLK, Display_DIN, Display_DC, Display_CS, Display_RST); // 84x48 pixels
TinyGPSPlus gps;
TinyGPSCustom gps_fix(gps, "GPGSA", 2);
TinyGPSCustom gps_pdop(gps, "GPGSA", 15);
//TinyGPSCustom gps_vdop(gps, "GPGSA", 17);

BMP085 baro = BMP085();
double QNH = 101325;
#define SAMPLES_ARR 10
static long k[SAMPLES_ARR];
long Pressure = QNH;
//float       Vario=0;
//int       samples=40;
//int       maxsamples=50;
//float     alt[30];
//float     tim[30];

struct config_t
{
  double lat = -23.977893;
  double lng = -46.315421;
} wp;

#define pi 3.141593

unsigned int wpHeading = 0;
float wpDistance = 0;
unsigned int best_pdop = 9999;

void setup()
{
  //Serial.begin(9600);
  display.begin();
  display.setContrast(50);
  display.setTextSize(2);
  display.setTextColor(BLACK, WHITE);
  display.clearDisplay();
  display.println(" FlyBy  GPS");
  display.display();

  pinMode(BT_ESQ_PIN, INPUT_PULLUP);
  pinMode(BT_DIR_PIN, INPUT_PULLUP);

  ss.begin(9600);
  Wire.begin();
  baro.init(MODE_ULTRA_HIGHRES, QNH, false);
  EEPROM_readAnything(0, wp);

  smartDelay(2000);
}

static void draw(unsigned char tela, boolean esquerda)
{
  char buffer[9];
  if (tela == 0) // compass
  {
    unsigned char compassX = 23;
    unsigned char compassY = 24;
    unsigned char compassSZ = 20;

    if (!esquerda)
      compassX = 61;

    unsigned char x;
    unsigned char y;
    unsigned int degree = 0;
    if (gps.location.isValid())
    {
      degree = 360 - gps.course.deg();
      wpHeading = (unsigned int)TinyGPSPlus::courseTo(gps.location.lat(), gps.location.lng(), wp.lat, wp.lng);
      wpDistance = TinyGPSPlus::distanceBetween(gps.location.lat(), gps.location.lng(), wp.lat, wp.lng) / 1000;
      display.drawLine(
          ((cos((wpHeading + degree + 270) * (pi / 180))) * (compassSZ - 5)) + compassX,
          ((sin((wpHeading + degree + 270) * (pi / 180))) * (compassSZ - 5)) + compassY,
          ((cos((wpHeading + degree + 270 + 130) * (pi / 180))) * (compassSZ - 10)) + compassX,
          ((sin((wpHeading + degree + 270 + 130) * (pi / 180))) * (compassSZ - 10)) + compassY,
          BLACK);

      display.drawLine(
          ((cos((wpHeading + degree + 270) * (pi / 180))) * (compassSZ - 5)) + compassX,
          ((sin((wpHeading + degree + 270) * (pi / 180))) * (compassSZ - 5)) + compassY,
          ((cos((wpHeading + degree + 270 + 230) * (pi / 180))) * (compassSZ - 10)) + compassX,
          ((sin((wpHeading + degree + 270 + 230) * (pi / 180))) * (compassSZ - 10)) + compassY,
          BLACK);
    }

    display.setTextSize(1);
    display.setTextColor(BLACK, WHITE);

    display.drawCircle(compassX, compassY, compassSZ, BLACK); // x0, y0, r, c

    x = ((cos(degree * (pi / 180))) * compassSZ) + compassX;
    y = ((sin(degree * (pi / 180))) * compassSZ) + compassY;
    display.setCursor(x - 2, y - 3);
    display.println("E");

    x = ((cos((degree + 90) * (pi / 180))) * compassSZ) + compassX;
    y = ((sin((degree + 90) * (pi / 180))) * compassSZ) + compassY;
    display.setCursor(x - 2, y - 3);
    display.println("S");

    x = ((cos((degree + 180) * (pi / 180))) * compassSZ) + compassX;
    y = ((sin((degree + 180) * (pi / 180))) * compassSZ) + compassY;
    display.setCursor(x - 2, y - 3);
    display.println("W");

    x = ((cos((degree + 270) * (pi / 180))) * compassSZ) + compassX;
    y = ((sin((degree + 270) * (pi / 180))) * compassSZ) + compassY;
    display.setCursor(x - 2, y - 3);
    display.println("N");
  }
  else if (tela == 1) // speed
  {

    if (gps.speed.isValid())
      dtostrf(gps.speed.kmph(), 0, 0, buffer);
    infobox("Speed", "km/h", buffer, gps.speed.isValid(), esquerda);
  }
  else if (tela == 2) // Baro Altitude
  {
    dtostrf((double)getAlt(), 0, 0, buffer);
    infobox("Altit.", "meters", buffer, true, esquerda);
  }
  else if (tela == 3) // WP Distance
  {
    if (gps.location.isValid())
    {
      wpHeading = (unsigned int)TinyGPSPlus::courseTo(gps.location.lat(), gps.location.lng(), wp.lat, wp.lng);
      wpDistance = TinyGPSPlus::distanceBetween(gps.location.lat(), gps.location.lng(), wp.lat, wp.lng) / 1000;
      if (wpDistance > 90)
        dtostrf(wpDistance, 0, 1, buffer);
      else
        dtostrf(wpDistance, 0, 0, buffer);
    }
    infobox("Distan", "Km", buffer, gps.location.isValid(), esquerda);
  }
  else if (tela == 4) // Temperature
  {
    dtostrf(getTemp(), 0, 0, buffer);
    infobox("Temper.", "^C", buffer, true, esquerda);
  }
}

static void infobox(char *tit, char *sub, char *value, boolean valid, boolean esquerda)
{
  unsigned char x = 0;
  if (!esquerda)
    x = 42;
  display.setCursor(x, 0);
  display.setTextSize(1);
  display.println(tit);

  if (valid)
    printCenter(value, x + 21, 24, 2);
  else
    printCenter("*", x + 21, 24, 2);

  display.setCursor(x, 40);
  display.setTextSize(1);
  display.print(sub);
}

static void main_screen()
{
  unsigned char t_esq = 0, t_dir = 1, tmax = 4, bt;
  do
  {
    display.clearDisplay();
    draw(t_esq, true);
    draw(t_dir, false);
    display.display();

    if (BT_ESQ_STATUS && BT_DIR_STATUS)
    {
      BT_ESQ_STATUS = BT_DIR_STATUS = false;
      menu_screen();
    }
    else if (BT_ESQ_STATUS)
    {
      ++t_esq;
      if (t_esq > tmax)
        t_esq = 0;
    }
    else if (BT_DIR_STATUS)
    {
      ++t_dir;
      if (t_dir > tmax)
        t_dir = 0;
    }
    BT_ESQ_STATUS = BT_DIR_STATUS = false;

    smartDelay(10);

  } while (true);
}

static void menu_screen()
{
  unsigned char op = 0, opmax = 2;
  unsigned char t = 0, tmax = 2;
  unsigned int lat_d, lat_m, lat_s;
  boolean lat_f;
  unsigned int lng_d, lng_m, lng_s;
  boolean lng_f;
  boolean editando = false;
  char lat_buf[11];
  char lng_buf[12];
  char lat_fields[] = {0, 2, 3, 5, 6, 8, 9};
  char lat_fields_t[] = {'X', '9', '9', '9', '9', '9', '9'};
  char lng_fields[] = {0, 2, 3, 4, 6, 7, 9, 10};
  char lng_fields_t[] = {'X', '9', '9', '9', '9', '9', '9', '9'};

  do
  {
    display.clearDisplay();
    display.setTextSize(1);
    if (t == 0)
    {
      opmax = 3;
      if (op == 0)
        display.setTextColor(WHITE, BLACK);
      else
        display.setTextColor(BLACK, WHITE);
      display.println("Waypoint");

      if (op == 1)
        display.setTextColor(WHITE, BLACK);
      else
        display.setTextColor(BLACK, WHITE);
      display.println("Infos");

      if (op == 2)
        display.setTextColor(WHITE, BLACK);
      else
        display.setTextColor(BLACK, WHITE);
      display.println("Altitude");

      if (op == opmax)
        display.setTextColor(WHITE, BLACK);
      else
        display.setTextColor(BLACK, WHITE);
      display.println("Back");

      if (BT_ESQ_STATUS && BT_DIR_STATUS)
      {
        if (op == opmax)
        {
          BT_ESQ_STATUS = BT_DIR_STATUS = false;
          main_screen();
        }
        else if (op == 0)
        {
          t = 1;
          toDMS(wp.lat, &lat_f, &lat_d, &lat_m, &lat_s);
          toDMS(wp.lng, &lng_f, &lng_d, &lng_m, &lng_s);
          sprintf(lat_buf, "%c %02d.%02d.%02d", lat_f ? 'S' : 'N', lat_d, lat_m, lat_s);
          sprintf(lng_buf, "%c %03d.%02d.%02d", lng_f ? 'W' : 'E', lng_d, lng_m, lng_s);
        }
        else if (op == 1)
        {
          BT_ESQ_STATUS = BT_DIR_STATUS = false;
          op = opmax;
          //data_screen();
        }
      }
      else if (BT_DIR_STATUS)
      {
        ++op;
        if (op > opmax)
          op = 0;
      }
      else if (BT_ESQ_STATUS)
      {
        if (op == 0)
          op = opmax;
        else
          --op;
      }
      BT_ESQ_STATUS = BT_DIR_STATUS = false;
    }
    else if (t == 1)
    {
      opmax = 16;

      display.setTextColor(BLACK, WHITE);
      display.println("LAT:");

      for (int i = 0; i < 10; ++i)
      {
        if (lat_fields[op] == i && (lat_fields_t[op] == '9' || lat_fields_t[op] == 'X'))
          display.setTextColor(WHITE, BLACK);
        else
          display.setTextColor(BLACK, WHITE);

        display.print(lat_buf[i]);
      }

      display.setTextColor(BLACK, WHITE);
      display.println("");
      display.println("LNG:");

      for (int i = 0; i < 11; ++i)
      {
        if (lng_fields[op - 7] == i && (lng_fields_t[op - 7] == '9' || lng_fields_t[op - 7] == 'X'))
          display.setTextColor(WHITE, BLACK);
        else
          display.setTextColor(BLACK, WHITE);

        display.print(lng_buf[i]);
      }

      display.println("");

      if (editando)
      {
        display.setTextColor(WHITE, BLACK);
        display.print("   EDITING   ");
      }
      else
      {
        if (op == 15)
          display.setTextColor(WHITE, BLACK);
        else
          display.setTextColor(BLACK, WHITE);
        display.print("SAVE");

        display.setTextColor(BLACK, WHITE);
        display.print("   ");

        if (op == 16)
          display.setTextColor(WHITE, BLACK);
        else
          display.setTextColor(BLACK, WHITE);
        display.print("BACK");
      }

      char *fields = lat_fields;
      char *fields_t = lat_fields_t;
      char *buf = lat_buf;
      char n = op;
      if (n > 6)
      {
        n -= 7;
        fields = lng_fields;
        fields_t = lng_fields_t;
        buf = lng_buf;
      }

      if (BT_ESQ_STATUS && BT_DIR_STATUS)
      {
        if (op == opmax)
        {
          BT_ESQ_STATUS = BT_DIR_STATUS = false;
          main_screen();
        }
        else if (op == opmax - 1)
        {
          BT_ESQ_STATUS = BT_DIR_STATUS = false;

          lat_d = ((lat_buf[lat_fields[1]] - '0') * 10) + (lat_buf[lat_fields[2]] - '0');
          lat_m = ((lat_buf[lat_fields[3]] - '0') * 10) + (lat_buf[lat_fields[4]] - '0');
          lat_s = ((lat_buf[lat_fields[5]] - '0') * 10) + (lat_buf[lat_fields[6]] - '0');

          lng_d = ((lng_buf[lng_fields[1]] - '0') * 100) + ((lng_buf[lng_fields[2]] - '0') * 10) + (lng_buf[lng_fields[3]] - '0');
          lng_m = ((lng_buf[lng_fields[4]] - '0') * 10) + (lng_buf[lng_fields[5]] - '0');
          lng_s = ((lng_buf[lng_fields[6]] - '0') * 10) + (lng_buf[lng_fields[7]] - '0');

          wp.lat = lat_buf[lat_fields[0]] == 'S' ? ((((double)lat_s / 60) + (double)lat_m) / 60 + lat_d) * -1 : (((double)lat_s / 60) + (double)lat_m) / 60 + lat_d;
          wp.lng = lng_buf[lng_fields[0]] == 'W' ? ((((double)lng_s / 60) + (double)lng_m) / 60 + lng_d) * -1 : (((double)lng_s / 60) + (double)lng_m) / 60 + lng_d;
          EEPROM_writeAnything(0, wp);
          main_screen();
        }
        else
        {
          if (editando)
            editando = false;
          else
            editando = true;
        }
      }
      else if (BT_ESQ_STATUS)
      {
        if (editando)
        {
          if (fields_t[n] == '9')
          {
            if (buf[fields[n]] > '0')
              --buf[fields[n]];
            else
              buf[fields[n]] += 9;
          }
          else if (fields_t[n] == 'X' && op < 7)
          {
            if (buf[fields[n]] == 'S')
              buf[fields[n]] = 'N';
            else
              buf[fields[n]] = 'S';
          }
          else if (fields_t[n] == 'X' && op > 6)
          {
            if (buf[fields[n]] == 'W')
              buf[fields[n]] = 'E';
            else
              buf[fields[n]] = 'W';
          }
        }
        else
        {
          if (op == 0)
            op = opmax;
          else
            --op;
        }
      }
      else if (BT_DIR_STATUS)
      {
        if (editando)
        {
          if (fields_t[n] == '9')
          {
            if (buf[fields[n]] < '9')
              ++buf[fields[n]];
            else
              buf[fields[n]] -= 9;
          }
          else if (fields_t[n] == 'X' && op < 7)
          {
            if (buf[fields[n]] == 'S')
              buf[fields[n]] = 'N';
            else
              buf[fields[n]] = 'S';
          }
          else if (fields_t[n] == 'X' && op > 6)
          {
            if (buf[fields[n]] == 'W')
              buf[fields[n]] = 'E';
            else
              buf[fields[n]] = 'W';
          }
        }
        else
        {
          ++op;
          if (op > opmax)
            op = 0;
        }
      }
      BT_ESQ_STATUS = BT_DIR_STATUS = false;
    }

    display.display();
    smartDelay(10);
  } while (true);
}

static void data_screen()
{
  unsigned char t = 0, tmax = 2;
  do
  {
    display.clearDisplay();
    display.setTextSize(1);

    if (t == 0)
    {
      display.print("Sats:");
      display.print(gps.satellites.value()); //gps.satellites.isValid()
      display.print(" fix:");
      display.println(gps_fix.value());

      display.print("PDOD:");
      display.println(TinyGPSPlus::parseDecimal(gps_pdop.value()), DEC);

      display.print("HDOP:");
      display.println(gps.hdop.value()); //

      display.print("VDOP:");
      //display.println(gps_vdop.value());

      printDate(gps.date);
      display.println("");

      printTime(gps.time);
    } /*
    else if(t==1){
      //display.print("lat:");
      display.println(gps.location.lat(), 6);

      //display.print("Lng:");
      display.println(gps.location.lng(), 6);

      display.print("Speed:");
      display.println(gps.speed.kmph());

      display.print("Alt GPS:");
      display.println(gps.altitude.meters());

      display.print("Head:");
      display.println(gps.course.deg());
    }
    else if(t==2){
      display.print("WP Dist:");
      display.println(wpDistance, 1);

      display.print("WP Head:");
      display.println(wpHeading);

      display.print("Baro Alt:");
      display.println(getAlt());

      display.print("Pres:");
      display.println(Pressure);

      display.print("Temp:");
      display.println(getTemp());
    }*/

    display.display();

    if (BT_ESQ_STATUS && BT_DIR_STATUS)
    {
      BT_ESQ_STATUS = BT_DIR_STATUS = false;
      main_screen();
    }
    else if (BT_ESQ_STATUS)
    {
      ++t;
      if (t > tmax)
        t = 0;
    }
    else if (BT_DIR_STATUS)
    {
      if (t == 0)
        t = tmax;
      else
        --t;
    }
    BT_ESQ_STATUS = BT_DIR_STATUS = false;

    //++t_dir;
    //if(t_dir>tmax) t_dir=0;
    smartDelay(100);
    //} while(gps.location.isValid());
  } while (true);
}

static void gps_check_screen()
{
  unsigned char s = 0;
  char c[4] = {'-', '\\', '|', '/'};
  do
  {
    display.clearDisplay();
    display.setTextSize(1);
    display.print("Waiting GPS ");
    display.print(c[s]);
    if (s > 2)
      s = 0;
    else
      s++;

    display.setCursor(0, 7);
    display.print("Sats:");
    gps.satellites.isValid() ? display.print(gps.satellites.value(), DEC) : display.print('*');
    display.print(" fix:");
    display.print(gps_fix.value());

    display.setCursor(0, 14);
    display.print("Alt:");
    display.print(getAlt(), DEC);

    display.setCursor(0, 21);
    display.print("Temp:");
    display.print(getTemp());

    display.setCursor(0, 28);
    display.print("Baro:");
    display.print(Pressure, DEC);

    display.setCursor(0, 35);
    printTime(gps.time);

    display.display();

    if (BT_ESQ_STATUS && BT_DIR_STATUS)
    {
      BT_ESQ_STATUS = BT_DIR_STATUS = false;
      main_screen();
    }
    BT_ESQ_STATUS = BT_DIR_STATUS = false;

    smartDelay(1000);
  } while (!gps.location.isValid());
}

void loop()
{
  gps_check_screen();
  main_screen();
}

static void barovario()
{
  //float time=millis();              //take time, look into arduino millis() function
  //float N1=0;float N2=0;float N3=0; float D1=0;float D2=0;
  long pressure;
  baro.calcTruePressure(&pressure);      //get one sample from baro in every loop
  Pressure = Averaging_Filter(pressure); //put it in filter and take average, this averaging is for NMEA output
  /*
  float altitude = (float)44330 * (1 - pow(((float)pressure/p0), 0.190295));  //take new altitude in meters from pressure sample, not from average pressure

  for(int cc=1;cc<=maxsamples;cc++){    //vario algorithm
    alt[(cc-1)]=alt[cc];                //move "altitude" value in every loop inside table
    tim[(cc-1)]=tim[cc];                //move "time" value in every loop inside table
  };                                    //now we have altitude-time tables
  alt[maxsamples]=altitude;             //put current "altitude" value on the end of the table
  tim[maxsamples]=time;                 //put current "time" value on the end of the table
  float stime=tim[maxsamples-samples];
  for(int cc=(maxsamples-samples);cc<maxsamples;cc++){
    N1+=(tim[cc]-stime)*alt[cc];
    N2+=(tim[cc]-stime);
    N3+=(alt[cc]);
    D1+=(tim[cc]-stime)*(tim[cc]-stime);
    D2+=(tim[cc]-stime);
  };

  Vario=1000*((samples*N1)-N2*N3)/(samples*D1-D2*D2);
  */
}

static int getAlt()
{
  return 44330 * (1 - pow(((float)Pressure / QNH), 0.190295));
}

static int getTemp()
{
  long Temperature;
  baro.getTemperature(&Temperature); // get temperature in celsius from time to time, we have to divide that by 10 to get XY.Z
  return int(Temperature / 10);
}

static void bt_check()
{
  if (!BT_ESQ_STATUS && digitalRead(BT_ESQ_PIN) == LOW && !BT_ESQ_PRESTATUS)
    BT_ESQ_PRESTATUS = true;
  else if (!BT_ESQ_STATUS && digitalRead(BT_ESQ_PIN) == HIGH && BT_ESQ_PRESTATUS)
  {
    BT_ESQ_PRESTATUS = false;
    BT_ESQ_STATUS = true;
  }
  if (!BT_DIR_STATUS && digitalRead(BT_DIR_PIN) == LOW && !BT_DIR_PRESTATUS)
    BT_DIR_PRESTATUS = true;
  else if (!BT_DIR_STATUS && digitalRead(BT_DIR_PIN) == HIGH && BT_DIR_PRESTATUS)
  {
    BT_DIR_PRESTATUS = false;
    BT_DIR_STATUS = true;
  }
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    bt_check();
    barovario();
    while (ss.available())
      gps.encode(ss.read());

    unsigned int pdop = gps_pdop.isValid() ? TinyGPSPlus::parseDecimal(gps_pdop.value()) : 9999;
    if (gps_pdop.isValid() && pdop < best_pdop)
    {
      best_pdop = pdop;
      if (gps.altitude.isValid())
      {
        QNH = Pressure / pow((1 - (float)gps.altitude.value() / 4433000), 5.255);
      }
    }
  } while (millis() - start < ms);
}

static long Averaging_Filter(long input) // moving average filter function
{
  long sum = 0;
  for (int i = 0; i < SAMPLES_ARR; i++)
  {
    k[i] = k[i + 1];
  }
  k[SAMPLES_ARR - 1] = input;
  for (int i = 0; i < SAMPLES_ARR; i++)
  {
    sum += k[i];
  }
  return (sum / SAMPLES_ARR);
}

static void printFloatCenter(float val, bool valid, int prec, unsigned int x, unsigned int y, unsigned char charsize)
{
  display.setTextSize(charsize);
  char line[32] = "*";
  unsigned char linesz = 1;
  unsigned int i;

  if (valid)
  {
    if (prec == 1)
      linesz = sprintf(line, "%ld.%ld", long(val), long((val - long(val)) * 10));
    else if (prec == 2)
      linesz = sprintf(line, "%ld.%ld", long(val), (val - long(val)) * 100);
    else if (prec == 6)
      linesz = sprintf(line, "%ld.%ld", long(val), (val - long(val)) * 1000000);
    else
      linesz = sprintf(line, "%ld", long(val));
  }

  unsigned char linewidth = (linesz * charsize * 5) / 2;
  for (i = 0; i < linesz; i++)
  {
    display.setCursor(x + ((i * ((charsize * 5))) - linewidth), y - (charsize * 4));
    display.print(line[i]);
  }
}

static void printCenter(char *c, unsigned char x, unsigned char y, unsigned char charsize)
{
  unsigned int sz = strlen(c);
  unsigned char linewidth = (sz * charsize * 5) / 2;
  display.setTextSize(charsize);
  for (unsigned int i = 0; i < sz; i++)
  {
    display.setCursor(x + ((i * ((charsize * 5))) - linewidth), y - (charsize * 4));
    display.print(c[i]);
  }
}

static void printDate(TinyGPSDate &d)
{
  if (!d.isValid())
  {
    display.print(F("**********"));
  }
  else
  {
    char sz[11];
    sprintf(sz, "%02d-%02d-%02d", d.year(), d.month(), d.day());
    display.print(sz);
  }
}

static void printTime(TinyGPSTime &t)
{
  if (!t.isValid())
  {
    display.print(F("********"));
  }
  else
  {
    char sz[9];
    sprintf(sz, "%02d:%02d:%02d", t.hour(), t.minute(), t.second());
    display.print(sz);
  }
}

static void toDMS(double coord, boolean *negative, unsigned int *d, unsigned int *m, unsigned int *s)
{
  if (coord < 0)
  {
    *negative = true;
    coord *= -1; // troca o sinal da coordenada
  }
  else
    *negative = false;

  while (coord < -180.0)
    coord += 360.0;

  while (coord > 180.0)
    coord -= 360.0;

  *d = (int)coord;
  coord -= *d;
  *m = coord * 60;
  coord = (coord * 60) - *m;
  *s = coord * 60;
}
